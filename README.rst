Web Sync
========

This is a web-based synchronization backend for the OpenLP remote sync plugin.


Table of Contents
-----------------

- `Installation <#installation>`_
- `License <#license>`_

Installation
------------

The best way to install and use this is via Docker containers.

Docker CLI
~~~~~~~~~~

.. code-block:: shell

   docker run -p 8000:8000 registry.gitlab.com/openlp/web-sync:latest

Docker Compose
~~~~~~~~~~~~~~

.. code-block:: yaml

   services:
     web-sync:
       image: registry.gitlab.com/openlp/web-sync:latest
       env:
         - DATABASE_URL=postgresql://user:password@postgres/web-sync
         - SECRET_KEY=yoursecretkeyhere
       ports:
         - "127.0.0.1:8000:8000"
       restart: unless-stopped
     postgres:
       image: postgres:15
       env:
         - POSTGRES_USER=user
         - POSTGRES_PASSWORD=password
         - POSTGRES_DB=web-sync
       volumes:
         - "./data/postgres:/var/lib/postgresql/data"
       restart: unless-stopped


Configuration
-------------

The web-sync application is configured through environment variables. Here are the options available:

+-----------------------------+-----------------------------------------------------------------+---------------+
| Option                      | Description                                                     | Default value |
+=============================+=================================================================+===============+
| DATABASE_URL                | The URL for the database. This is an `SQLAlchemy database URI`_ | ``sqlite://`` |
+-----------------------------+-----------------------------------------------------------------+---------------+
| DATABASE_ECHO               | Print out database commands. This should be off in production   | False         |
+-----------------------------+-----------------------------------------------------------------+---------------+
| SECRET_KEY                  | This is the key used to encrpyt session cookies and the like.   |               |
|                             | This should never be blank.                                     |               |
+-----------------------------+-----------------------------------------------------------------+---------------+

Generating a secret key
~~~~~~~~~~~~~~~~~~~~~~~

To generate a secure string for your secret key, you can use the ``secrets`` module in Python, like so:

.. code-block:: shell

   python3 -c 'import secrets; print(secrets.token_urlsafe(48))'


License
-------

``web-sync`` is distributed under the terms of the `MIT <https://spdx.org/licenses/MIT.html>`_ license.
