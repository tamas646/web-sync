FROM python:3

WORKDIR /usr/src/app

COPY . .
COPY .git .git

RUN pip install hatch

RUN hatch env create

CMD [ "hatch", "run", "start" ]
EXPOSE 5000/tcp
