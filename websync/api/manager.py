from websync.helpers import db
from websync.db.models import Collection, Entity, EntityChange, OpType
from quart import abort
from quart.utils import run_sync
from datetime import datetime, timezone
from sqlalchemy.orm import Query
from sqlalchemy import select, delete

""" Helper functions """

def _get_collection(collection_id: int) -> Collection:
	"""
	Get the Collection entry from the database by id
	"""
	session = db.bind.Session()

	# request the collection from the SQL database
	collection = session.get(Collection, collection_id)

	session.close()

	# raise error if not found
	if collection == None:
		raise Exception(f'Collection with id \'{collection_id}\' not found')

	return collection


def _format_timestamp(t: datetime) -> str:
	"""
	Convert datetime in local timezone to ISO 8601 format
	"""
	return t.astimezone().isoformat()


def _sync_changes(collection_id: int, data: dict):
	session = db.bind.Session()

	collection: Collection = session.get(Collection, collection_id)
	if collection == None:
		raise Exception(f'Collection with id \'{collection_id}\' not found')

	# check ctag conflict
	if collection.ctag != data['ctag']:
		abort(409)

	# todo: do not allow future timestamps

	# submit changes
	for entity_change in data['changes']:
		result: Query = session.query(Entity).filter_by(uuid=entity_change['uuid'])
		entity: Entity = None if result.count() == 0 else result.one()

		match entity_change['operation']:
			case 'created':
				if entity != None and not entity.deleted: # todo: check user_id to be the same
					abort(409)

				if entity != None:
					# recreate entity which has been deleted in the past
					entity.deleted = False
					entity.timestamp = datetime.fromisoformat(entity_change['timestamp'])
					entity.data = entity_change['data']
				else:
					# create the new entity
					entity = Entity(
						uuid=entity_change['uuid'],
						timestamp=datetime.fromisoformat(entity_change['timestamp']),
						data=entity_change['data'],
						collection_id=collection_id,
					)
					session.add(entity)

				# flush to get newly created entity id
				session.flush()

				# create entity change entry
				changeItem = EntityChange(
					entity_id=entity.id,
					timestamp=datetime.fromisoformat(entity_change['timestamp']),
					collection_id=collection_id,
					collection_ctag=collection.ctag + 1,
					operation=OpType.created,
				)
				session.add(changeItem)

			case 'modified':
				if entity == None or entity.deleted: # todo: check user_id to be the same
					abort(409)
				if entity.timestamp.astimezone() > datetime.fromisoformat(entity_change['timestamp']):
					abort(409)

				# modify the entity
				entity.timestamp = datetime.fromisoformat(entity_change['timestamp'])
				entity.data = entity_change['data']

				# create entity change entry
				changeItem = EntityChange(
					entity_id=entity.id,
					timestamp=datetime.fromisoformat(entity_change['timestamp']),
					collection_id=collection_id,
					collection_ctag=collection.ctag + 1,
					operation=OpType.modified,
				)
				session.add(changeItem)

			case 'deleted':
				if entity == None or entity.deleted: # todo: check user_id to be the same
					abort(409)
				if entity.timestamp.astimezone() > datetime.fromisoformat(entity_change['timestamp']):
					abort(409)

				# delete the entity
				entity.deleted = True

				# create entity change entry
				changeItem = EntityChange(
					entity_id=entity.id,
					timestamp=datetime.fromisoformat(entity_change['timestamp']),
					collection_id=collection_id,
					collection_ctag=collection.ctag + 1,
					operation=OpType.deleted,
				)
				session.add(changeItem)

		# todo: make use of insert multiple feature of SQL for better performance

		# flush at the end of entity operation to remain consistent
		session.flush()

	# update ctag
	collection.ctag = collection.ctag + 1
	collection.timestamp = datetime.fromisoformat(data['timestamp']) # todo: do not trust client timestamp -> calculate it

	# flush and commit the changes
	session.flush()
	session.commit()

	session.close()


def _get_changes(collection_id: int, since_ctag: int) -> dict:
	session = db.bind.Session()

	collection: Collection = session.get(Collection, collection_id)
	if collection == None:
		raise Exception(f'Collection with id \'{collection_id}\' not found')

	# collect and aggregate changes
	changes_by_uuid = {}

	if int(collection.ctag) > since_ctag:
		# request changed entities
		result = session.execute(
		 	select(Entity.uuid, EntityChange.operation, EntityChange.timestamp, Entity.data) \
				.join_from(EntityChange, Entity) \
				.filter(EntityChange.collection_id == collection.id, EntityChange.collection_ctag > since_ctag) \
				.order_by(EntityChange.collection_ctag, EntityChange.timestamp)
		)

		for row in result:
			if row.uuid in changes_by_uuid:
				# entity is already in the dictionary
				match (changes_by_uuid[row.uuid]['operation'], row.operation.name):
					# (previous optype, current optype)
					case ('created', 'created'):
						raise Exception(f'Error in EntityChanges: {row.uuid} created twice')
					case ('created', 'modified'):
						changes_by_uuid[row.uuid]['timestamp'] = _format_timestamp(row.timestamp)
						changes_by_uuid[row.uuid]['data'] = row.data
					case ('created', 'deleted'):
						del changes_by_uuid[row.uuid]

					case ('modified', 'created'):
						raise Exception(f'Error in EntityChanges: {row.uuid} created after modified')
					case ('modified', 'modified'):
						changes_by_uuid[row.uuid]['timestamp'] = _format_timestamp(row.timestamp)
						changes_by_uuid[row.uuid]['data'] = row.data
					case ('modified', 'deleted'):
						changes_by_uuid[row.uuid]['operation'] = 'deleted'
						changes_by_uuid[row.uuid]['timestamp'] = _format_timestamp(row.timestamp)
						changes_by_uuid[row.uuid]['data'] = None

					case ('deleted', 'created'):
						changes_by_uuid[row.uuid]['operation'] = 'modified'
						changes_by_uuid[row.uuid]['timestamp'] = _format_timestamp(row.timestamp)
						changes_by_uuid[row.uuid]['data'] = row.data
					case ('deleted', 'modified'):
						raise Exception(f'Error in EntityChanges: {row.uuid} modified after deleted')
					case ('deleted', 'deleted'):
						raise Exception(f'Error in EntityChanges: {row.uuid} deleted twice')

			else:
				# entity is new in the dictionary
				changes_by_uuid[row.uuid] = {
					'uuid': row.uuid,
					'operation': row.operation.name,
					'timestamp': _format_timestamp(row.timestamp),
					'data': row.data,
				}

	session.close()

	# create the output array of changes: sort entity dictionary values by timestamp
	changes = sorted(changes_by_uuid.values(), key=lambda e: e['timestamp'])

	return {
		'ctag': collection.ctag,
		'timestamp': _format_timestamp(collection.timestamp),
		'changes': changes,
	}


""" API endpoint handlers """

async def get_sync_status(collection_id: int):
	"""
	Handle the GET request made to `/sync-status/{collection}`
	"""
	collection: Collection = await run_sync(_get_collection)(collection_id)

	return {
		'ctag': collection.ctag,
		'timestamp': _format_timestamp(collection.timestamp),
	}


async def get_sync(collection_id: int, since_ctag: int):
	"""
	Handle the GET request made to `/sync/{collection}`
	"""
	return await run_sync(_get_changes)(collection_id, since_ctag)


async def put_sync(collection_id: int, request_data: dict):
	"""
	Handle the PUT request made to `/sync/{collection}`
	"""
	# todo: check against a schema

	# todo: check timestamps (cannot be futuristic)

	await run_sync(_sync_changes)(collection_id, request_data)

	collection: Collection = await run_sync(_get_collection)(collection_id)

	return {
		'ctag': collection.ctag,
		'timestamp': _format_timestamp(collection.timestamp),
	}
