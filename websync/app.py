from quart import Quart, request, abort
from websync.helpers import bcrypt, db
from websync.db import models
from websync.api import manager as api
from websync.web import manager as web

app = Quart(__name__)

""" Bind the Quart extensions to the app """
bcrypt.init_app(app)
db.init_app(app)


def run():
    """
    This is the main entrypoint of the project which runs the Quart app
    """
    models.init_schema()
    app.run()


@app.route('/')
async def hello():
    """
    This function responds to the requests to /
    """
    return 'hello'


""" API endpoints """
api_path = '/api/v1'
available_collection_ids = {
    'songs': 1, # name: id
}

@app.route(f'{api_path}/sync-status/<string:collection>')
async def sync_status(collection: str):
    if collection not in available_collection_ids:
        abort(404)

    return await api.get_sync_status(available_collection_ids[collection])


@app.route(f'{api_path}/sync/<string:collection>', methods=['GET', 'PUT'])
async def sync(collection: str):
    if collection not in available_collection_ids:
        abort(404)

    if request.method == 'GET':
        return await api.get_sync(available_collection_ids[collection], int(request.args.get('since_ctag')))

    elif request.method == 'PUT':
        # parse request body as json
        request_data = (await request.get_json()) # aborts with 400 Bad Request if cannot parse
        return await api.put_sync(available_collection_ids[collection], request_data)


if __name__ == '__main__':
    run()
