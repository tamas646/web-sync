from quart_bcrypt import Bcrypt
from quart_sqlalchemy import SQLAlchemyConfig
from quart_sqlalchemy.framework import QuartSQLAlchemy

""" Bcrypt library """
BCRYPT_HANDLE_LONG_PASSWORDS = True
bcrypt = Bcrypt()

""" Database config """
db = QuartSQLAlchemy(
	config=SQLAlchemyConfig(
		binds=dict(
			default=dict(
				engine=dict(
					url='sqlite:///websync.sqlite',
					echo=True,
					connect_args=dict(check_same_thread=False),
				),
				session=dict(
					expire_on_commit=False,
				),
			)
		)
	)
)
