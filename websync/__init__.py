"""
The :mod:`websync` module is the core module of the project.
It contains all project functionality of the Web Sync app.
"""
