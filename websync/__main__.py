from websync import app

"""
This is the entrypoint of the project if runned via `python -m`
It calls the main entrypoint found in `app.py`
"""
app.run()
