from websync.helpers import bcrypt, db
from sqlalchemy import String, ForeignKey, UniqueConstraint, func, Enum
from sqlalchemy.orm import Mapped, mapped_column, relationship
from typing import List
from datetime import datetime
import enum

""" Models """

class RoleType(enum.Enum):
	admin = 1
	# todo: more roles

	def __str__(self):
		match self.value:
			case 1: return 'Admin'
			# todo: more roles

	def __repr__(self):
		return f'RoleType.{self.name})'

class User(db.Model):
	"""
	User model
	"""
	__tablename__ = 'user'

	id: Mapped[int] = mapped_column(primary_key=True)
	username: Mapped[str] = mapped_column(unique=True)
	hashed_password: Mapped[str] = mapped_column(String(60))
	role: Mapped[int] = mapped_column(Enum(RoleType), nullable=False)
	api_keys: Mapped[List['ApiKey']] = relationship(back_populates='user')
	collections: Mapped[List['Collection']] = relationship(back_populates='user')

	def __init__(self, username: str, **kwargs):
		super(User, self).__init__(**kwargs)
		self.username = username

	async def setPassword(self, password: str):
		self.hashed_password = (await bcrypt.async_generate_password_hash(password, 10)).decode('utf-8')

	async def checkPassword(self, password: str) -> bool:
		return await bcrypt.check_password_hash(self.hashed_password, password)

	def __repr__(self):
		return f'User(id={self.id!r}, username={self.username!r}, hashed_password={self.hashed_password!r})'


class ApiKey(db.Model):
	"""
	ApiKey model
	"""
	__tablename__ = 'apikey'

	id: Mapped[int] = mapped_column(primary_key=True)
	key: Mapped[str] = mapped_column(String(32), unique=True)
	user_id = mapped_column(ForeignKey('user.id'), nullable=False)
	user: Mapped['User'] = relationship(back_populates='api_keys')

	def __repr__(self):
		return f'ApiKey(id={self.id!r}, key={self.key!r}, user_id={self.user_id!r})'


class Collection(db.Model):
	"""
	Collection model
	"""
	__tablename__ = 'collection'

	id: Mapped[int] = mapped_column(primary_key=True)
	name: Mapped[str] = mapped_column()
	ctag: Mapped[int] = mapped_column(default=0)
	timestamp: Mapped[datetime] = mapped_column(server_default=func.current_timestamp())
	user_id = mapped_column(ForeignKey('user.id'), nullable=False)
	user: Mapped['User'] = relationship(back_populates='collections')
	entities: Mapped[List['Entity']] = relationship(back_populates='collection')
	changes: Mapped[List['EntityChange']] = relationship(back_populates='collection')
	__table_args__ = (UniqueConstraint('name', 'user_id', name='collection_uix_1'),)

	def __repr__(self):
		return f'Collection(id={self.id!r}, name={self.name!r}, ctag={self.ctag!r}, timestamp={self.timestamp!r})'


class Entity(db.Model):
	"""
	Entity model
	"""
	__tablename__ = 'entity'

	id: Mapped[int] = mapped_column(primary_key=True)
	uuid: Mapped[str] = mapped_column(String(36), unique=True)
	created: Mapped[datetime] = mapped_column(server_default=func.current_timestamp())
	timestamp: Mapped[datetime]
	data: Mapped[str]
	deleted: Mapped[bool] = mapped_column(default=False)
	collection_id = mapped_column(ForeignKey('collection.id'), nullable=False)
	collection: Mapped['Collection'] = relationship(back_populates='entities')
	changes: Mapped[List['EntityChange']] = relationship(back_populates='entity')

	def __repr__(self):
		return f'Entity(id={self.id!r}, uuid={self.uuid!r}, created={self.created!r}, timestamp={self.timestamp!r}, data={self.data!r}, user_id={self.user_id!r}, collection_id={self.collection_id!r})'


class OpType(enum.Enum):
	created = 1
	modified = 2
	deleted = 3

	def __repr__(self):
		return f'OpType.{self.name})'

class EntityChange(db.Model):
	"""
	EntityChange model
	"""
	__tablename__ = 'entity_change'

	collection_id = mapped_column(ForeignKey('collection.id'), nullable=False)
	collection: Mapped['Collection'] = relationship(back_populates='changes')
	collection_ctag: Mapped[int]
	entity_id = mapped_column(ForeignKey('entity.id'), primary_key=True)
	entity: Mapped['Entity'] = relationship(back_populates='changes')
	operation = mapped_column(Enum(OpType), nullable=False)
	timestamp: Mapped[datetime] = mapped_column(primary_key=True)

	def __repr__(self):
		return f'EntityChange(collection_id={self.collection_id!r}, collection_ctag={self.collection_ctag!r}, entity_id={self.entity_id!r}, operation={self.operation!r}, timestamp={self.timestamp!r})'


def init_schema():
	""" Create SQL tables if required """

	db.create_all()


	""" Add default db entries if required """

	with db.bind.Session() as session:
		# Add default user
		u1 = session.get(User, 1)
		if not u1:
			u1 = User('admin', id=1, role=RoleType.admin)
			u1.hashed_password = bcrypt.generate_password_hash('admin', 10).decode('utf-8')
			print(f'Adding user: {u1!r}')
			session.add(u1)

		# Add songs collection
		songs = session.get(Collection, 1)
		if not songs:
			songs = Collection(id=1, name='songs', user=u1)
			print(f'Adding collection: {songs!r}')
			session.add(songs)

		session.commit()
		session.close()
